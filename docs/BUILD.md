REF:
- <https://gitlab.com/kalilinux/nethunter/apps/kali-nethunter-store-client/-/blob/nethunter/docs/BUILD.md>
- <https://gitlab.com/kalilinux/nethunter/apps/kali-nethunter-store-privileged-extension/-/blob/nethunter/docs/BUILD.md>

- - -

On the host, get ready with:

```console
docker run --interactive --tty --rm --volume /tmp/:/mnt/tmp debian:9-slim
cd ~/
```

- - -

On the container, setup network apt, as Debian 9 is EOL and Debian docker doesn't enable `contrib`:

```console
source /etc/os-release
sed -i "s|deb.debian.org|archive.debian.org|g;
          s|security.debian.org|archive.debian.org|g;
           /${VERSION_CODENAME}-updates/d;
           /${VERSION_CODENAME}\/updates/d;
          s/main$/main contrib non-free/g" /etc/apt/sources.list
apt-get update
```

- - - 

Install Java version _(otherwise, could use Debian sid (aka Unstable) or `temurin-*-jdk`)_:

```console
apt-cache search openjdk | awk '/openjdk-[0-9]*-jdk / {print $1}'
apt-get install --yes openjdk-8-jdk
update-alternatives --list java
update-alternatives --set java /usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java
update-alternatives --list javac
update-alternatives --set javac /usr/lib/jvm/java-8-openjdk-amd64/bin/javac
```

<!--
            = Java Major version 45 = Java 1.1
            = Java Major version 46 = Java 1.2
            = Java Major version 47 = Java 1.3
            = Java Major version 48 = Java 1.4
            = Java Major version 49 = Java 5
            = Java Major version 50 = Java 6
            = Java Major version 51 = Java 7  - openjdk-7 - Debian 8 (Jessie)
Gradle 2.0  = Java Major version 52 = Java 8  - openjdk-8 - Debian 9 (Stretch)
Gradle 4.3  = Java Major version 53 = Java 9
Gradle 4.7  = Java Major version 54 = Java 10
Gradle 5.0  = Java Major version 55 = Java 11 - openjdk-11 - Debian 11 (bullseye) & Debian 10 (buster)
Gradle 5.4  = Java Major version 56 = Java 12
Gradle 6.0  = Java Major version 57 = Java 13
Gradle 6.3  = Java Major version 58 = Java 14
Gradle 6.7  = Java Major version 59 = Java 15
Gradle 7.0  = Java Major version 60 = Java 16
Gradle 7.3  = Java Major version 61 = Java 17 - openjdk-17 - Debian 12 (bookworm) & Debian 11 (bullseye)
Gradle 7.5  = Java Major version 62 = Java 18
Gradle 7.6  = Java Major version 63 = Java 19
Gradle 8.3  = Java Major version 64 = Java 20
Gradle 8.5  = Java Major version 65 = Java 21
Gradle 8.8  = Java Major version 66 = Java 22
Gradle 8.10 = Java Major version 67 = Java 23

REF: https://wiki.debian.org/Java
     https://docs.gradle.org/current/userguide/compatibility.html
-->

- - -

Install Android SDK:

```console
apt-get install --yes android-sdk
grep -q ANDROID_HOME ~/.bashrc || echo export ANDROID_HOME=/usr/lib/android-sdk >> ~/.bashrc       # Fix: > The SDK directory '/opt/android-sdk' does not exist.
grep -q ANDROID_SDK_ROOT ~/.bashrc || echo export ANDROID_SDK_ROOT=\${ANDROID_HOME} >> ~/.bashrc   # Fix: > SDK location not found. Define location with an ANDROID_SDK_ROOT environment variable..
source ~/.bashrc

## Fix: /usr/share/maven-repo/com/android/tools/build/gradle/2.2.2/gradle-2.2.2.jar
apt-get install --yes libgradle-android-plugin-java

## sdkmanager gets packed up in Debian 11 (bullseye backports)
apt-get install --yes git python3-minimal  python3-requests
git clone --depth=1 --branch=0.5.1 https://gitlab.com/fdroid/sdkmanager.git /opt/sdkmanager/
git -C /opt/sdkmanager/ checkout -B master 0e9b843482124fab502fd1b21ab1ca8e2e4c90c5
```

- - -

Get the Android app (Kali NetHunter Store Privileged Extension):

```console
apt-get install --yes git
git clone https://gitlab.com/kalilinux/nethunter/apps/kali-nethunter-store-privileged-extension.git ~/kali-nethunter-store-privileged-extension/
cd ~/kali-nethunter-store-privileged-extension/
```

- - -

Finish up with SDK/app's packages/dependencies:

```console
COMPILED_SDK=$( sed -n 's_.*compileSdkVersion\s*\([0-9]*\).*_\1_p' ./app/build.gradle )
BUILD_TOOLS=$( sed -n "s_.*buildToolsVersion\s*'\([0-9.]*\)'.*_\1_p" ./app/build.gradle )
/opt/sdkmanager/sdkmanager.py --verbose "build-tools;${BUILD_TOOLS}" "platforms;android-${COMPILED_SDK}"
yes | /opt/sdkmanager/sdkmanager.py --verbose  --licenses
```

<!-- alt: $ apt install android-sdk-platform-23 -->

- - -

Build:

```console
./gradlew assembleDebug
./gradlew assembleDebug --info
find . -name '*apk' -type f
./gradlew assembleRelease
find $( pwd ) -name '*release*.apk' -type f
```

<!-- $ ./gradlew assembleDebug != $ gradle assembleDebug -->

- - - 

Copy out the build output (Android app, `*.apk`):

```console
cp -v /root/kali-nethunter-store-privileged-extension/app/build/outputs/apk/NetHunterStorePrivilegedExtension-release-unsigned.apk /mnt/tmp/
```
